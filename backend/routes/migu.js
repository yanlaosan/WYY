var express = require('express');
var router = express.Router();
const axios = require('axios')

router.get('/list', function(req, res, next) {
  // console.log(req)
  let url = req.query.migu_url
  // console.log(req.query);
  let {playListType,playListId,contentCount,pid,albumId } = req.query
  axios.get(url, {
    headers: {
      referer: 'https://m.music.migu.cn/v3',  //源域名
    },
    params:{
      playListType,
      playListId,
      contentCount,
      pid,
      albumId
    }
  }).then((response) => {
    res.json(response.data)
  }).catch((e) => {
    // console.log(e)
  })
});

module.exports = router;

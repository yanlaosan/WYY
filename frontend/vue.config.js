const path = require('path');
module.exports = {
  publicPath:'/dist',
  outputDir:'dist',
  assetsDir:'static',
  configureWebpack: {
    resolve: {
      // 别名
      alias: {
        'src': path.resolve(__dirname, './src'),
        '@a': path.resolve(__dirname, './src/assets'),
        "@c": path.resolve(__dirname, './src/components'),
        '@v': path.resolve(__dirname, './src/views'),
        '@u': path.resolve(__dirname, './src/utils')
      }
    }
  },
  devServer:{
    proxy:{
      "/api": {
        target: "http://localhost:3000",
        changeOrigin: true,
      },
      "/ap": {
        target: "http://localhost:3000",
        changeOrigin: true,
        pathRewrite: {
          "^/ap": "",
        },
      }
    }
  },
  // css: {
  //   loaderOptions: {
  //     less: {
  //       // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
  //       lessOptions: {
  //         modifyVars: {
  //           // 直接覆盖变量
  //           "@skeleton-row-height":"49",
  //           // 'text-color': '#111',
  //           // 'border-color': '#eee',
  //           // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
  //           // hack: `true; @import "./src/views/Hot/hot.less";`,
  //         },
  //       },
  //     },
  //   },
  // },
}
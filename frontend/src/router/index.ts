import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const Home = ()=>import('@v/Home.vue')
const Hot = ()=> import('@v/Hot/Hot.vue')
const Recom = ()=> import('@v/Recommend/Recom.vue')
const Search = ()=> import('@v/Search/Search.vue')
const SongList = ()=> import('@v/Recommend/SongList.vue')
const MusicPlay = ()=> import('@v/Music/MusicPlay.vue')
const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect:'/home'
  },
  {
    path: '/home',
    component:Home,
    redirect:'/hot',
    children:[
      {
        path: '/hot',
        component:Hot
      },
      {
        path: '/recom',
        component:Recom,
        meta:{
          title:'推荐歌单',
          keepAlive:true
        }
      },
      {
        path: '/search',
        component:Search,
        meta:{
          title:'搜索',
          keepAlive:true
        }
      }
    ]
  },
  {
    path:'/songList/',
    name:'songList',
    component:SongList,
  },
  {
    path:'/music/',
    name:'music',
    component:MusicPlay,
    beforeEnter:(to,from,next) => {
      console.log(from)
      next()
    }
  }

]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})




const originalPush:any = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router

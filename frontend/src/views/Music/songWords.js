function songWords(geci){
  var lrcStr = `${geci}`
  const audio = document.querySelector('.audio-player__audio') //注意：需要取到最底层的audio标签
  const span = document.querySelector('.geci>span')
  // 2. 绑定事件
  audio.addEventListener('timeupdate', function () {
  
    // 3. 拿到当前时间
    const { currentTime } = audio
  
    // 4. 格式化时间
    const minutes= parseInt(currentTime / 60)
    const seconds = parseInt(currentTime % 60)
    const timeStr= (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds)
  
    // 5. 组装正则表达式
    const reg = new RegExp('\\[' + timeStr + '\\.\\d{2}\\](.+)')
    let lrc = reg.exec(lrcStr)
    lrc = lrc ? lrc[1] : ''
  
    span.innerHTML = lrc ? lrc : span.innerHTML

    })
  }
  
  export { 
    songWords
   }
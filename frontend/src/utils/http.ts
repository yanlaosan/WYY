import axios from 'axios'
const instance = axios.create({
  timeout: 10000
})

instance.interceptors.request.use(function (config) {
  config['headers'] = {
    'content-type': 'application/json; charset=utf-8',
    // 'referer':'https://m.music.migu.cn/v3'
  }
  return config
})

interface postStar {
  url: string
  data:string
}
interface getStar {
  url: string
  params:object
}
const get = ({ url, params = {} }:getStar) => {
  return instance.get(
    url,
    { params },
  )
}
const post = ({ url, data = "" }: postStar) => {
  return instance.post(url, JSON.stringify(data))
}

export {
  post,
  get
}
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    searchResult:'',
    playingSong:{}
  },
  mutations: {
    change_searchResult(state,searchValue) {
      state.searchResult = searchValue;
    }
    // ,
    // change_playingSong(state,songInfo){
    //   state.playingSong = songInfo
    //   console.log('0',state.searchResult);

    // }
  },
  actions: {
  },
  modules: {
  }
})

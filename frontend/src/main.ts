import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { Skeleton, Swipe, SwipeItem } from 'vant';
import 'vant/lib/index.css';


import '@a/reset.css'
import '@a/stylus/clearfix.css'
import AudioPlayer from '@liripeng/vue-audio-player'
import '@liripeng/vue-audio-player/lib/vue-audio-player.css'

Vue.use(AudioPlayer)
Vue.use(Skeleton)
Vue.use(Swipe)
Vue.use(SwipeItem)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
